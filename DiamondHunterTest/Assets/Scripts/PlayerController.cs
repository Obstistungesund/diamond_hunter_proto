﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[SerializeField] public float moveSpeed;
	[SerializeField] public float gemSpeed;
	//[SerializeField] private GameObject listHelper;

	//private List<GameObject> listHelperList;

	private KeyCode lastKeyCode;

	// GemTrail
	[SerializeField] public float gemTrailLength;
	private int gemsInScene;
	[HideInInspector] public enum dir {up, left, down, right};
	[HideInInspector] public List<dir> directionList;
	[HideInInspector] public List<Vector3> movementList;
	[HideInInspector] public List<float> movementDistance;

	// Use this for initialization
	void Start () {

		//listHelperList = new List<GameObject>();
		//listHelperList.Add (Instantiate (listHelper));

		gemsInScene = GameObject.FindGameObjectsWithTag ("Gem").Length;
		lastKeyCode = KeyCode.W;
		//print (gemsInScene);
		movementList = new List<Vector3>();
		movementList.Add (transform.position);

		directionList = new List<dir>();
		directionList.Add (dir.up);

		movementDistance = new List<float>();
		movementDistance.Add (0);
	}


	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.Q)) {
			
			GemController.removeFromLine(2);

		}
		//Debug.LogError ("ListCountPlayer " + movementList.Count);
		MovePlayer ();

		float allSum = 0;
		foreach (float sum in movementDistance) {

			allSum += sum;

		}
		//print (allSum);
		//print (movementList.Count);
	}

	void MovePlayer() {

		Vector3 position = transform.position;
		KeyCode lastLastKeyCode = lastKeyCode;
		 
		if (Input.GetKeyDown (KeyCode.W)) {

			lastKeyCode = KeyCode.W;

		} else if (Input.GetKeyDown (KeyCode.A)) {

			lastKeyCode = KeyCode.A;

		} else if (Input.GetKeyDown (KeyCode.S)) {

			lastKeyCode = KeyCode.S;

		} else if (Input.GetKeyDown (KeyCode.D)) {

			lastKeyCode = KeyCode.D;

		}


		if (Input.GetKey (lastKeyCode)) {

			dir direction = dir.up; // Up = Default
			float movement = moveSpeed * Time.deltaTime;
			bool directionChange = (lastKeyCode == lastLastKeyCode) ? false : true;

			switch (lastKeyCode) {

			case KeyCode.W:
				position.y += movement;
				transform.position = position;
				direction = dir.up;
				break;

			case KeyCode.A:
				position.x -= movement;
				transform.position = position;
				direction = dir.left;
				break;

			case KeyCode.S:
				position.y -= movement;
				transform.position = position;
				direction = dir.down;
				break;

			case KeyCode.D:
				position.x += movement;
				transform.position = position;
				direction = dir.right;
				break;

			default:
				Debug.LogError ("Undefined Direction!");
				return;

			}

			if (directionChange) {
				
				movementList.Add (position);
				directionList.Add (direction);
				movementDistance.Add(movement);
				//listHelperList.Add (Instantiate(listHelper));

				movementListsManager ();

			} else {
				movementList[movementList.Count - 1] =  position;
				movementDistance[movementDistance.Count - 1] += movement;
				//listHelperList [listHelperList.Count - 1].transform.position = this.transform.position;
			}


		} else {

			// Der Spieler bleibt stehen (Pause)

		}


			
	}

	void movementListsManager() {
		
		//print (movementList.Count);

		float added = (gemsInScene * gemTrailLength) + 1;

		float sum = 0;
		bool sumExceeded = false;
		for (int i = movementList.Count - 1; i >= 0; i--) {

			if (sumExceeded) {

				movementList. RemoveAt (i);
				directionList.RemoveAt (i);
				movementDistance. RemoveAt (i);
				GemController.movementListIndexUpdater (-1);

				//Destroy (listHelperList[i]);
				//listHelperList.RemoveAt (i);

			} else {

				if (i == 0) {
					//print ("Sum: " + sum);
					return;
				}
				
				if (directionList [i] == dir.up || directionList [i] == dir.down) {

					sum += Mathf.Abs(movementList [i].y - movementList [i - 1].y);

				} else {

					sum += Mathf.Abs(movementList [i].x - movementList [i - 1].x);

				}

				sumExceeded = (sum > added) ? true : false;

			}

		}

		//print ("Sum: " + sum);

	}

}
