﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemController : MonoBehaviour {


	private static GameObject player;
	private static PlayerController playerController;
	private static float gemDistance;

	private bool isInLine;
	private int LineIndex;

	private static List<Vector3> movementList;
	private static List<PlayerController.dir> directionList;
	private static List<float> movementDistance;
	private static List<GemController> gems;
	private int movementListIndex;
	private static float gemSpeed;
	[HideInInspector] public static int gemsInLine;

	// Use this for initialization
	void Start () {

		gemsInLine = 0;
		gems = new List<GemController> ();

		player = GameObject.Find ("Player");
		playerController = player.GetComponent<PlayerController> ();

		gemDistance = playerController.gemTrailLength;
		isInLine = false;


		gemSpeed = playerController.gemSpeed;

		if (gemSpeed <= playerController.moveSpeed) 
			Debug.LogError ("The gems are slower then the Player!");

	}
		

	void addGemToLine() {

		gemsInLine++;
		movementList = playerController.movementList;
		directionList = playerController.directionList;
		movementDistance = playerController.movementDistance;
		
		movementListIndex = movementList.Count - 1;
		gems.Insert (0, this);

		transform.position = player.transform.position;

		isInLine = true;

		LineIndex = 0;

		for (int i = 1; i < gems.Count; i++) {

			gems [i].LineIndex += 1;

		}
	

	}

	public static void movementListIndexUpdater(int indexUpdate) {

		foreach(GemController gem in gems) {

			gem.movementListIndex += indexUpdate;

		}

	}

	public static void removeFromLine(int index) {
		gemsInLine--;
		print ("ERROOOOOOR!");
		GemController removeGem = gems [index];

		int removeLineIndex = removeGem.LineIndex;
		foreach(GemController gem in gems) {
			if(gem.LineIndex > removeLineIndex) {
				//print("New Index: " + (gem.LineIndex - 1));
				gem.LineIndex--;
			}
		}

		removeGem.isInLine = false;
		gems.RemoveAt (gems.IndexOf(removeGem));

	}

	// Update is called once per frame
	void Update () {



		if (isInLine) {

			float added = gemDistance * (LineIndex + 1);
			float speed = gemSpeed * Time.deltaTime;

			print (movementListIndex + ", " + movementList.Count);

			float distanceToPlayer = 0;
			for (int i = movementList.Count - 1; i > movementListIndex; i--) {
				distanceToPlayer += movementDistance [i];
			}

			//print ("FirstDistanceToPlayer: " + distanceToPlayer);

			float distanceToNextPoint = 0;
			switch (directionList [movementListIndex]) {

			case PlayerController.dir.up:
				distanceToNextPoint = movementList [movementListIndex].y - transform.position.y;
				break;

			case PlayerController.dir.left:
				distanceToNextPoint = transform.position.x - movementList [movementListIndex].x;
				break;

			case PlayerController.dir.down:
				distanceToNextPoint = transform.position.y - movementList [movementListIndex].y;
				break;

			case PlayerController.dir.right:
				distanceToNextPoint = movementList [movementListIndex].x - transform.position.x;
				break;

			}


			distanceToPlayer += distanceToNextPoint;
			//print (distanceToPlayer);
			float movement = Mathf.Min (distanceToPlayer - added, speed);

			if(movement > 0) {

				print (true);
				Vector3 newPosition;

				if (distanceToNextPoint > movement) {

					newPosition = transform.position;
					PlayerController.dir direction = directionList [movementListIndex];

					switch (direction) {

					case PlayerController.dir.up:
						newPosition.y += movement;
						break;

					case PlayerController.dir.left:
						newPosition.x -= movement;
						break;

					case PlayerController.dir.down:
						newPosition.y -= movement;
						break;

					case PlayerController.dir.right:
						newPosition.x += movement;
						break;
					}

					//Debug.Break();
					transform.position = newPosition;

				} else {

					int i = movementListIndex + 1;
					print("LoopTest: " + (distanceToNextPoint + movementDistance[i]));
					float pointSum = distanceToNextPoint + movementDistance [i];
					do {
						
						if (pointSum >= movement) {

							print (pointSum);
							newPosition = movementList[i - 1];
							PlayerController.dir direction = directionList [i];

							pointSum -= movementDistance [i];

							switch (direction) {

							case PlayerController.dir.up:
								newPosition.y += (movement - pointSum);
								break;

							case PlayerController.dir.left:
								newPosition.x -= (movement - pointSum);
								break;

							case PlayerController.dir.down:
								newPosition.y -= (movement - pointSum);
								break;

							case PlayerController.dir.right:
								newPosition.x += (movement - pointSum);
								break;
							}

							movementListIndex = i;
							transform.position = newPosition;
							//Debug.Break();
							break;
						}

					} while(pointSum <= movement);

					/*for (float pointSum = distanceToNextPoint + movementDistance[i]; pointSum <= movement; ++i, pointSum += movementDistance [i]) {
						print ("pointSum");
						if (pointSum >= movement) {

							print (pointSum);
							newPosition = movementList[i - 1];
							PlayerController.dir direction = directionList [i];

							pointSum -= movementDistance [i];

							switch (direction) {

							case PlayerController.dir.up:
								newPosition.y += (added - pointSum);
								break;

							case PlayerController.dir.left:
								newPosition.x -= (added - pointSum);
								break;

							case PlayerController.dir.down:
								newPosition.y -= (added - pointSum);
								break;

							case PlayerController.dir.right:
								newPosition.x += (added - pointSum);
								break;
							}

							movementListIndex = i;
							transform.position = newPosition;
							//Debug.Break();
							break;
						}
					}*/
				}
			}
		}

	}

	void OnTriggerEnter2D(Collider2D other) {
		
		if (!isInLine && other.gameObject == GameObject.FindWithTag ("Player")) {
			addGemToLine ();
		}

	}
}
