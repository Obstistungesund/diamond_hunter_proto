﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[SerializeField] public float moveSpeed;
	[SerializeField] public float gemSpeed;

	// GemTrail
	[SerializeField] public float gemTrailLength;
	private int gemsInScene;
	[HideInInspector] public enum dir {up, left, down, right};
	[HideInInspector] public List<dir> directionList;
	[HideInInspector] public List<Vector3> movementList;
	[HideInInspector] public List<float> movementDistance;


	[SerializeField] private GameObject stars; //Prefab
	private GameObject instansStars; 		   //Runtime Stars


	[SerializeField] public float timeForDig;
	[SerializeField] public float explosionTime;
	private float timeLeft;


	private Animator anim;


	private TileManager tileManager;
	private SoundManager soundManager;


	private List<KeyCode> movementPressed;
	private KeyCode lastDirKeyCode;


	void Start () {
		timeLeft = 0;
		tileManager = GameObject.Find ("TileManager").GetComponent<TileManager> ();
		soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager> ();
		
		gemsInScene = GameObject.FindGameObjectsWithTag ("Gem").Length;
		lastDirKeyCode = KeyCode.None;

		movementList = new List<Vector3>();
		movementList.Add (transform.position);

		directionList = new List<dir>();
		directionList.Add (dir.up);

		movementDistance = new List<float>();
		movementDistance.Add (0);

		movementPressed = new List<KeyCode> ();

		anim = GetComponent<Animator> ();

		enemyPositionSetter ();
	}


	void Update () {

		if(Input.GetKeyDown(KeyCode.Q)) {

			GemController.removeFromLine(2);

		}

		timeLeft -= Time.deltaTime;

		PlayerDirection ();
			
	}

	void FixedUpdate() {
		
		if (movementPressed.Count != 0 && Input.GetKey (movementPressed[movementPressed.Count - 1]) && timeLeft <= 0) {
			anim.SetBool ("isDigging", false);
			PlayerMovement ();

		} else {

			if(timeLeft <= 0) {
				anim.SetBool ("isDigging", false);
				anim.SetInteger ("direction", 5);
				//Der Spieler bleibt stehen (Pause)
			}

		}
	}


	void PlayerDirection() {
		
		if (movementPressed.Count != 0) {
			for (int i = 0; i < movementPressed.Count; i++) {
				if (Input.GetKeyUp (movementPressed[i]))
					movementPressed.RemoveAt (i);
			}
		}

		if (Input.GetKeyDown (KeyCode.W)) {
			movementPressed.Add(KeyCode.W);
			//GetComponentInChildren<PlayerTriggerController> ().rotateCollider (dir.up);
		} 

		if (Input.GetKeyDown (KeyCode.A)) {
			movementPressed.Add (KeyCode.A);
			//GetComponentInChildren<PlayerTriggerController> ().rotateCollider (dir.left);
		}

		if (Input.GetKeyDown (KeyCode.S)) {
			movementPressed.Add (KeyCode.S);
			//GetComponentInChildren<PlayerTriggerController> ().rotateCollider (dir.down);
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			movementPressed.Add (KeyCode.D);
			//GetComponentInChildren<PlayerTriggerController> ().rotateCollider (dir.right);
		}

	}


	void PlayerMovement() {

		Vector3 position = transform.position;

		/*if (!walkSound.isPlaying) {
			walkSound.Play();
		}*/

		KeyCode thisDirKey = movementPressed[movementPressed.Count - 1];

		dir direction = dir.up; // Up = Default
		float movement = moveSpeed * Time.deltaTime;
		bool directionChange = (thisDirKey == lastDirKeyCode) ? false : true;

		switch (thisDirKey) {

		case KeyCode.W:
			position.y += movement;
			transform.position = position;
			direction = dir.up;
			anim.SetInteger ("direction", 0);
			break;

		case KeyCode.A:
			position.x -= movement;
			transform.position = position;
			direction = dir.left;
			anim.SetInteger ("direction", 1);
			break;

		case KeyCode.S:
			position.y -= movement;
			transform.position = position;
			direction = dir.down;
			anim.SetInteger ("direction", 2);
			break;

		case KeyCode.D:
			position.x += movement;
			transform.position = position;
			direction = dir.right;
			anim.SetInteger ("direction", 3);
			break;

		default:
			Debug.LogError ("Undefined Direction!");
			return;

		}

		if (directionChange) {

			movementList.Add (position);
			directionList.Add (direction);
			movementDistance.Add(movement);
				//listHelperList.Add (Instantiate(listHelper));

			movementListsManager ();

		} else {
			
			movementList[movementList.Count - 1] = position;
			movementDistance[movementDistance.Count - 1] += movement;
			//listHelperList [listHelperList.Count - 1].transform.position = this.transform.position;
		}

		lastDirKeyCode = thisDirKey;

	}


	void gameOver() {
		moveSpeed = 0;
	}


	void enemyPositionSetter() {
		EnemyController.playerPosition = transform.position;
	}


	public void tileTrigger(Collider2D other) {
		
		if (other.tag == "Tile" && timeLeft <= -0.1F) {

			StartCoroutine (invokeTileDmg (other.transform.position));
			//invokeTileDmg (other.transform.position);
			//tileManager.tileDmg (other.transform.position);
			soundManager.playDiggingSound ();

			timeLeft = timeForDig;
			anim.SetBool ("isDigging", true);

		} else if (other.tag == "Respawn") {
			print ("Test");
		}
	}


	IEnumerator invokeTileDmg(Vector3 otherPosition)
	{
		//print (timeForDig);
		yield return new WaitForSeconds(timeForDig - 0.0F);

		tileManager.tileDmg (otherPosition);
		// Now do your thing here
	}


	void movementListsManager() {

		//print (movementList.Count);

		float added = (gemsInScene * gemTrailLength) + 1;

		float sum = 0;
		bool sumExceeded = false;
		for (int i = movementList.Count - 1; i >= 0; i--) {

			if (sumExceeded) {

				movementList.RemoveAt (i);
				directionList.RemoveAt (i);
				movementDistance.RemoveAt (i);
				GemController.movementListIndexUpdater (-1);

				//Destroy (listHelperList[i]);
				//listHelperList.RemoveAt (i);

			} else {

				if (i == 0) {
					//print ("Sum: " + sum);
					return;
				}

				if (directionList [i] == dir.up || directionList [i] == dir.down) {

					sum += Mathf.Abs (movementList [i].y - movementList [i - 1].y);

				} else {

					sum += Mathf.Abs (movementList [i].x - movementList [i - 1].x);

				}

				sumExceeded = (sum > added) ? true : false;

			}

		}
	}

	public void explosionDmg() {
		timeLeft = explosionTime;
	}

}
