﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {


	[HideInInspector] private Text scoreText;
	private int score;
	// Use this for initialization
	void Start () {
		score = PlayerPrefs.GetInt("score");
		scoreText = this.gameObject.GetComponent<Text> ();
		scoreText.text = "Score: " + score.ToString();
	}


	public void levelEnd() {
		scoreAdd((GameObject.Find ("Timer").GetComponent<Countdown> ().timeLeft * 10 + GemController.gemsInLine * ((GameObject.FindWithTag ("Enemy").GetComponent<EnemyController> ().isEscalated) ? 3 : 2)) * 10);

	}

	public void scoreAdd(int addedScore) {

		score += addedScore;
		PlayerPrefs.SetInt("score", score);
		scoreText.text = ("Score: " + score);

	}

}
