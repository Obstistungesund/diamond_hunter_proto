﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTileController : MonoBehaviour {

	[SerializeField] public float moveSpeed;
	[SerializeField] public float gemSpeed;

	// GemTrail
	[SerializeField] public float gemTrailLength;
	private int gemsInScene;
	[HideInInspector] public enum dir {up, left, down, right, none};
	[HideInInspector] public List<dir> directionList;
	[HideInInspector] public List<Vector3> movementList;
	[HideInInspector] public List<float> movementDistance;
	private dir lastDir = dir.none;


	[SerializeField] private GameObject stars; //Prefab
	private GameObject instansStars; 		   //Runtime Stars


	[SerializeField] public float timeForDig;
	[SerializeField] public float explosionTime;
	private float timeLeftForDig;
	private float timeLeftForStun;
	private bool isMoving;
	private bool isDigging;
	private bool digCancel;
	private Vector3 endPosition;
	private dir moveDirection;


	private Animator anim;


	private TileManager tileManager;
	private SoundManager soundManager;
	//private SoundManager soundManager;


	private List<KeyCode> movementPressed;


	void Start () {
		soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager>();
		digCancel = false;
		timeLeftForDig = 0;
		timeLeftForStun = 0;
		tileManager = GameObject.Find ("TileManager").GetComponent<TileManager> ();
		//soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager> ();

		//GameObject.Find("BlackScreenSpotLight").GetComponent<blackScreenZoom>().zoomToPlayerInvoke();

		gemsInScene = GameObject.FindGameObjectsWithTag ("Gem").Length;

		movementList = new List<Vector3>();
		movementList.Add (transform.position);

		directionList = new List<dir>();
		directionList.Add (dir.up);

		movementDistance = new List<float>();
		movementDistance.Add (0);

		movementPressed = new List<KeyCode> ();

		anim = GetComponent<Animator> ();

		endPosition = transform.position;
		moveDirection = dir.up;

		enemyPositionSetter ();

		//anim.SetInteger ("direction", 5);
		anim.SetBool ("isWalking", false);
	}


	void Update () {

		timeLeftForDig -= Time.deltaTime;
		timeLeftForStun -= Time.deltaTime;

		PlayerDirection ();

		//playerMovement ();
	}

	void FixedUpdate() {
		playerMovement ();
	}


	void PlayerDirection() {

		if (movementPressed.Count != 0) {
			for (int i = 0; i < movementPressed.Count; i++) {
				if (!Input.GetKey(movementPressed[i]))
					movementPressed.RemoveAt (i);
			}
		}

		if (Input.GetKeyDown (KeyCode.W)) {
			movementPressed.Add(KeyCode.W);
		} 

		if (Input.GetKeyDown (KeyCode.A)) {
			movementPressed.Add (KeyCode.A);
		}

		if (Input.GetKeyDown (KeyCode.S)) {
			movementPressed.Add (KeyCode.S);
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			movementPressed.Add (KeyCode.D);
		}

	}
		

	void playerMovement() {

		if (timeLeftForStun > 0) {
			//anim.SetInteger ("direction", 5);
			anim.SetBool ("isDigging", false);
			anim.SetBool ("isWalking", false);
			//StopCoroutine (invokeTileDmg (endPosition));
			digCancel = true;
			return;
		}

		dir movementPressedDir = dir.none;
		if (movementPressed.Count != 0) {

			switch (movementPressed [movementPressed.Count - 1]) {

			case KeyCode.W:
				movementPressedDir = dir.up;
				break;

			case KeyCode.A:
				movementPressedDir = dir.left;
				break;

			case KeyCode.S:
				movementPressedDir = dir.down;
				break;

			case KeyCode.D:
				movementPressedDir = dir.right;
				break;
			}
		}



		if (!isMoving) {

			// Is standing

			Vector3 nextEndPosition = this.transform.position;

			switch(movementPressedDir) {

			case dir.up:
				nextEndPosition.y++;
				anim.SetInteger ("direction", 0);
				break;

			case dir.left:
				nextEndPosition.x--;
				anim.SetInteger ("direction", 1);
				break;

			case dir.down:
				nextEndPosition.y--;
				anim.SetInteger ("direction", 2);
				break;

			case dir.right:
				nextEndPosition.x++;
				anim.SetInteger ("direction", 3);
				break;

			default:
				// None
				break;

			}

			if (nextEndPosition == this.transform.position) {
				return;
			}

			if (!tileManager.isWorldPosInWorldGrid (nextEndPosition) || tileManager.getGridFromWorldPoint (nextEndPosition).tag == "Wall" || tileManager.getGridFromWorldPoint (nextEndPosition).tag == "Border") {
				//anim.SetInteger ("direction", 5);
				anim.SetBool ("isWalking", false);
			} else {
				isMoving = true;
				anim.SetBool ("isWalking", true);
				endPosition = nextEndPosition;
				moveDirection = movementPressedDir;
				//lastDir = movementPressedDir;

				//movementListsManager ();
			}
		}


		if (isMoving) {

			bool returning = false;
			if (!(moveDirection == movementPressedDir || movementPressedDir == dir.none)) {
				
				if ((int)moveDirection >= 2) {
					if ((int)moveDirection - 2 == (int)movementPressedDir) {
						returning = true;
					}
				} else {
					if ((int)moveDirection + 2 == (int)movementPressedDir) {
						returning = true;
					}
				}

				if (isDigging == true) {
					returning = true;
				}

				if (returning) {

					// Turns around

					Vector3 backPosition = endPosition;
					anim.SetBool ("isDigging", false);
					//dir backDirection = dir.none;

					if (isDigging) {

						backPosition = transform.position;

						//StopCoroutine (invokeTileDmg (backPosition));
						digCancel = true;

						switch (movementPressedDir) {

						case dir.up:
							backPosition.y++;
							//backDirection = dir.down;
							anim.SetInteger ("direction", 0);
							break;

						case dir.left:
							backPosition.x--;
							//backDirection = dir.right;
							anim.SetInteger ("direction", 1);
							break;

						case dir.down:
							backPosition.y--;
							//backDirection = dir.up;
							anim.SetInteger ("direction", 2);
							break;

						case dir.right:
							backPosition.x++;
							//backDirection = dir.left;
							anim.SetInteger ("direction", 3);
							break;

						}
					} else {

						switch (moveDirection) {

						case dir.up:
							backPosition.y--;
							//backDirection = dir.down;
							anim.SetInteger ("direction", 2);
							break;

						case dir.left:
							backPosition.x++;
							//backDirection = dir.right;
							anim.SetInteger ("direction", 3);
							break;

						case dir.down:
							backPosition.y++;
							//backDirection = dir.up;
							anim.SetInteger ("direction", 0);
							break;

						case dir.right:
							backPosition.x--;
							//backDirection = dir.left;
							anim.SetInteger ("direction", 1);
							break;
						}
					}

					endPosition = backPosition;
					isDigging = false;
					moveDirection = movementPressedDir;

					if(!tileManager.isWorldPosInWorldGrid(endPosition) || tileManager.getGridFromWorldPoint(endPosition).tag == "Wall") {
						endPosition = transform.position;
					}
					//transform.position = Vector3.MoveTowards (transform.position, endPosition, moveSpeed * Time.deltaTime);
				}
			}


			if (moveDirection == movementPressedDir || movementPressedDir == dir.none || returning == false) {
				
				// Going forward
				if (timeLeftForDig <= -0.03F) {
					if (tileManager.getGridFromWorldPoint (endPosition).tag == "Tile") {
						// is digging
						StartCoroutine (invokeTileDmg (endPosition));

						timeLeftForDig = timeForDig;
						anim.SetBool ("isDigging", true);
						isDigging = true;

					} else {

						if (!tileManager.isWorldPosInWorldGrid (endPosition)) {
							//anim.SetInteger ("direction", 5);
							anim.SetBool ("isWalking", false);
							isMoving = false;
							return;
						}

						Vector3 nextPos = Vector3.MoveTowards (transform.position, endPosition, moveSpeed * Time.deltaTime);

						float distance = Vector3.Distance (nextPos, transform.position);

						//print (distance);

						transform.position = nextPos;

						if (moveDirection == lastDir) {
							movementList [movementList.Count - 1] = transform.position;
							movementDistance [movementDistance.Count - 1] += distance;
						} else {
							movementList.Add (this.transform.position);
							directionList.Add (movementPressedDir);
							movementDistance.Add (distance);
							movementListsManager ();
						}

						lastDir = moveDirection;

						if (endPosition == transform.position) {
							isMoving = false;
							//anim.SetInteger ("direction", 5);
							anim.SetBool ("isWalking", false);
							return;
						}
						isDigging = false;
						anim.SetBool ("isDigging", false);
					}
				}
			}
		} 
	}


	void gameOver() {
		moveSpeed = 0;
	}
		
	void enemyPositionSetter() {
		EnemyController.playerPosition = transform.position;
	}
		
	IEnumerator invokeTileDmg(Vector3 otherPosition) {
		yield return new WaitForSeconds(timeForDig - 0.01F);

		if (digCancel || tileManager.getGridFromWorldPoint(otherPosition).tag == "NoneTile") {
			digCancel = false;
		} else {
			soundManager.playDiggingSound ();
			tileManager.tileDmg (otherPosition);
		}
	}
		
	void movementListsManager() {

		//print (movementList.Count);

		float added = (gemsInScene * gemTrailLength) + 1;

		float sum = 0;
		bool sumExceeded = false;
		for (int i = movementList.Count - 1; i >= 0; i--) {

			if (sumExceeded) {

				movementList.RemoveAt (i);
				directionList.RemoveAt (i);
				movementDistance.RemoveAt (i);
				GemController.movementListIndexUpdater (-1);

				//Destroy (listHelperList[i]);
				//listHelperList.RemoveAt (i);

			} else {

				if (i == 0) {
					//print ("Sum: " + sum);
					return;
				}

				if (directionList [i] == dir.up || directionList [i] == dir.down) {

					sum += Mathf.Abs (movementList [i].y - movementList [i - 1].y);

				} else {

					sum += Mathf.Abs (movementList [i].x - movementList [i - 1].x);

				}

				sumExceeded = (sum > added) ? true : false;

			}

		}
	}
		
	public void explosionDmg() {
		anim.SetTrigger ("dynamiteHit");
		timeLeftForStun = explosionTime;
	}

	public void enemyHit() {
		anim.SetTrigger ("Hit");
		timeLeftForStun = 0.3F;
	}
}
