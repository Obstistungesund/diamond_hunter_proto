﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GemController : MonoBehaviour {

	public int scoreValue;
	public GameObject scoreAdderPrefab;

	private static GameObject player;
	private static PlayerTileController playerTileController;
	private static float gemDistance;

	private bool isInLine;
	private int LineIndex;
	private GameObject pointAdder;

	private static List<Vector3> movementList;
	private static List<PlayerTileController.dir> directionList;
	private static List<float> movementDistance;
	private static List<GemController> gems;
	private int movementListIndex;
	private static float gemSpeed;
	[HideInInspector] public static int gemsInLine;
	private static HatchScript hatchScript;

	private static SoundManager soundManager;
	private static ScoreManager scoreManager;
	Animator anim;

	// Use this for initialization
	void Start () {
		
		pointAdder = Instantiate (scoreAdderPrefab, this.transform.position, this.transform.rotation, GameObject.Find("LevelUI").transform);
		pointAdder.SetActive (false);

		//anim = this.GetComponent<Animator> ();
		hatchScript = GameObject.Find ("Hatch").GetComponent<HatchScript>();
		soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager>();
		scoreManager = GameObject.Find ("Score").GetComponent<ScoreManager>();

		gemsInLine = 0;
		gems = new List<GemController> ();

		player = GameObject.Find ("Player");
		playerTileController = player.GetComponent<PlayerTileController> ();

		gemDistance = playerTileController.gemTrailLength;
		isInLine = false;


		gemSpeed = playerTileController.gemSpeed;

		if (gemSpeed <= playerTileController.moveSpeed) 
			Debug.LogError ("The gems are slower then the Player!");

	}


	void addGemToLine() {
		
		pointAdder.transform.position = this.transform.position;
		pointAdder.GetComponent<Text> ().text = scoreValue.ToString();
		pointAdder.GetComponent<Text> ().color = Color.white;
		pointAdder.SetActive (true);
		this.down = false;
		InvokeRepeating ("pointAdderAdd", 0.01F, 0.01F);

		scoreManager.scoreAdd (scoreValue);
		soundManager.playGemTaken();

		this.tag = "GemLine";
		gemsInLine++;

		hatchScript.gemsInLineUpdate (gemsInLine);
		movementList = playerTileController.movementList;
		directionList = playerTileController.directionList;
		movementDistance = playerTileController.movementDistance;

		movementListIndex = movementList.Count - 1;
		gems.Insert (0, this);

		transform.position = player.transform.position;

		isInLine = true;

		LineIndex = 0;

		for (int i = 1; i < gems.Count; i++) {

			gems [i].LineIndex += 1;

		}


	}

	public static void movementListIndexUpdater(int indexUpdate) {

		foreach(GemController gem in gems) {

			gem.movementListIndex += indexUpdate;

		}
	}

	public static void removeFromLine(int index) {

		gemsInLine--;
		hatchScript.gemsInLineUpdate (gemsInLine);

		GemController removeGem = gems [index];
		removeGem.gameObject.tag = "Gem";

		removeGem.down = true;
		removeGem.pointAdder.GetComponent<Text> ().text = "-" + removeGem.scoreValue.ToString();
		removeGem.pointAdder.GetComponent<Text> ().color = Color.red;
		removeGem.pointAdder.SetActive (true);
		removeGem.pointAdder.transform.position = removeGem.transform.position;
		removeGem.InvokeRepeating ("pointAdderAdd", 0.01F, 0.01F);


		print (removeGem.pointAdder.activeInHierarchy);
		int removeLineIndex = removeGem.LineIndex;
		foreach(GemController gem in gems) {
			if(gem.LineIndex > removeLineIndex) {
				gem.LineIndex--;
			}
		}

		removeGem.isInLine = false;
		gems.Remove (removeGem);

	}

	public static void removeFromLine(GameObject gem) {
		scoreManager.scoreAdd (-gem.GetComponent<GemController>().scoreValue);
		GemController thisGemInLine = null;

		foreach (GemController gemLine in gems) {
			if (gem == gemLine.gameObject) {
				thisGemInLine = gemLine;
			}
		}

		if (thisGemInLine != null) {
			removeFromLine (thisGemInLine.LineIndex);
		}
	}

	protected bool down = false;
	private float speed = 0.05F;
	private float test = 0.001F;
	private void pointAdderAdd() {
		speed -= test;
		Vector3 newPosition = pointAdder.transform.position;
		newPosition.y += down ? -speed : speed;

		if (speed <= 0) {
			pointAdder.SetActive (false);
			speed = 0.05F;
			CancelInvoke ();
			return;
		}

		pointAdder.transform.position = newPosition;
	}
		
	void Update () {



		if (isInLine) {

			float added = gemDistance * (LineIndex + 1);
			float speed = gemSpeed * Time.deltaTime;


			float distanceToPlayer = 0;
			for (int i = movementList.Count - 1; i > movementListIndex; i--) {
				distanceToPlayer += movementDistance [i];
			}
				

			float distanceToNextPoint = 0;
			switch (directionList [movementListIndex]) {

			case PlayerTileController.dir.up:
				distanceToNextPoint = movementList [movementListIndex].y - transform.position.y;
				break;

			case PlayerTileController.dir.left:
				distanceToNextPoint = transform.position.x - movementList [movementListIndex].x;
				break;

			case PlayerTileController.dir.down:
				distanceToNextPoint = transform.position.y - movementList [movementListIndex].y;
				break;

			case PlayerTileController.dir.right:
				distanceToNextPoint = movementList [movementListIndex].x - transform.position.x;
				break;

			}


			distanceToPlayer += distanceToNextPoint;
			float movement = Mathf.Min (distanceToPlayer - added, speed);

			if(movement > 0) {

				Vector3 newPosition;

				if (distanceToNextPoint > movement) {

					newPosition = transform.position;
					PlayerTileController.dir direction = directionList [movementListIndex];

					switch (direction) {

					case PlayerTileController.dir.up:
						newPosition.y += movement;
						break;

					case PlayerTileController.dir.left:
						newPosition.x -= movement;
						break;

					case PlayerTileController.dir.down:
						newPosition.y -= movement;
						break;

					case PlayerTileController.dir.right:
						newPosition.x += movement;
						break;
					}

					//Debug.Break();
					transform.position = newPosition;

				} else {

					int i = movementListIndex + 1;
					//print("LoopTest: " + (distanceToNextPoint + movementDistance[i]));
					float pointSum = distanceToNextPoint + movementDistance [i];
					do {

						if (pointSum >= movement) {
							
							newPosition = movementList[i - 1];
							PlayerTileController.dir direction = directionList [i];

							pointSum -= movementDistance [i];

							switch (direction) {

							case PlayerTileController.dir.up:
								newPosition.y += (movement - pointSum);
								break;

							case PlayerTileController.dir.left:
								newPosition.x -= (movement - pointSum);
								break;

							case PlayerTileController.dir.down:
								newPosition.y -= (movement - pointSum);
								break;

							case PlayerTileController.dir.right:
								newPosition.x += (movement - pointSum);
								break;
							}

							movementListIndex = i;
							transform.position = newPosition;

							break;
						}

						i--;
						pointSum += movementDistance[i];

					} while(pointSum <= movement);
				}
			}
		}

	}

	void OnTriggerEnter2D(Collider2D other) {

		if (!isInLine && other.gameObject == GameObject.FindWithTag ("Player")) {
			addGemToLine ();
			//anim.SetTrigger ("PickUp");

		}

	}
}
