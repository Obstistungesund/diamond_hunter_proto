﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DynamiteController : MonoBehaviour {

	[SerializeField] private KeyCode dynamitePress;
	[SerializeField] private float timeToExplode;
	[SerializeField] private float coolDown;
	//[SerializeField] private Sprite normalDynamite;
	//[SerializeField] private Sprite explodingDynamite;

	private TileManager tileManager;
	//private bool isDynamiteOnGround;
	private bool isDynamiteExploding;
	private Transform player;

	private PolygonCollider2D coll;
	private SpriteRenderer spriteRenderer;
	private Animator animator;

	private float timeLeftToExplode;
	private float runtimeCoolDown;

	private float coolDownUITime;

	// Use this for initialization
	void Start () {
		dynamiteTimeLeftText = GameObject.Find ("DynamiteTimeLeft");
		dynamiteTimeLeftText.SetActive (false);
		coolDownUITime = coolDown - timeToExplode;

		tileManager = GameObject.Find ("TileManager").GetComponent<TileManager>();
		player = GameObject.FindGameObjectWithTag ("Player").transform;

		coll = this.GetComponent<PolygonCollider2D> ();
		coll.enabled = false;
		colliderList = new List<Collider2D> ();

		spriteRenderer = this.GetComponent<SpriteRenderer> ();
		animator = this.GetComponent<Animator> ();

		timeLeftToExplode = 0;
		runtimeCoolDown = 0;

		spriteRenderer.sortingLayerName = "Tiles";
		spriteRenderer.sortingOrder = 1;

		isDynamiteExploding = false;

	}
	
	// Update is called once per frame
	void Update () {
		timeLeftToExplode -= Time.deltaTime;
		runtimeCoolDown -= Time.deltaTime;

		if (runtimeCoolDown < coolDownUITime && runtimeCoolDown > 0) {
			GameObject.Find ("DynamiteUISprite").GetComponent<Image> ().fillAmount = (coolDownUITime - runtimeCoolDown) / coolDownUITime;
		}

		if (Input.GetKeyDown (dynamitePress) && runtimeCoolDown <= 0) {

			//GameObject.FindWithTag ("Enemy").GetComponent<EnemyController>().dynamiteHit();
			GameObject.Find ("DynamiteUISprite").GetComponent<Image>().fillAmount = 0.0F;
			//isDynamiteOnGround = false;
			coll.enabled = true;
			//spriteRenderer.sprite = explodingDynamite;
			//isDynamiteExploding = true;
			timeLeftToExplode = timeToExplode;
			//animator.SetTrigger ("Exposianus");
			runtimeCoolDown = coolDown;
			spriteRenderer.sortingLayerName = "Foreground";
			
			animator.enabled = true;
			//isDynamiteOnGround = true;
			transform.position = new Vector3(Mathf.RoundToInt(player.position.x), Mathf.RoundToInt(player.position.y), -1);
			//spriteRenderer.sprite = normalDynamite

			dynamiteTimeLeftText.SetActive (true);
			dynamiteTimeLeftText.transform.position = this.transform.position;
			InvokeRepeating ("dynamiteTimeLeft", 0.01F, 0.01F);

		}

		if (timeLeftToExplode > 0) {

			if (timeLeftToExplode >= 5) {
				animator.speed = 1F;
			} else if (timeLeftToExplode >= 4) {
				animator.speed = 1.25F;
			} else if (timeLeftToExplode >= 3) {
				animator.speed = 1.5F;
			} else if (timeLeftToExplode >= 2) {
				animator.speed = 1.75F;
			} else if (timeLeftToExplode < 0.7F) {

				if(timeLeftToExplode < 0.2F)
					GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<ScreenShake> ().ShakeCamera ();
				spriteRenderer.sprite = null;
				animator.speed = 1;
				animator.SetTrigger ("Exposianus");
				isDynamiteExploding = true;
			}

		}

		if(timeLeftToExplode <= 0 && isDynamiteExploding) {
			
			tileManager.dynamiteExplosion (new Vector2Int(Mathf.RoundToInt(this.transform.position.x), Mathf.RoundToInt(this.transform.position.y)));
			isDynamiteExploding = false;
			transform.position = new Vector3 (300, 300, 0);
			spriteRenderer.sortingLayerName = "Tiles";
			//animator.SetBool ("Explosion", false);

			foreach (Collider2D col in colliderList) {

				if (col.tag == "Player") {
					col.GetComponent<PlayerTileController> ().explosionDmg ();
					GameObject.Find ("Lifes").GetComponent<LifesController>().damageTaken();
				} else if (col.tag == "Enemy") {
					
					col.GetComponent<EnemyController> ().dynamiteHit ();
					//col.GetComponent<EnemyController> ().enemyStun ();
					col.GetComponent<EnemyController> ().dropGem ();
				}
			}

			coll.enabled = false;
		}
	}

	private List<Collider2D> colliderList;
	void OnTriggerStay2D(Collider2D other) {
		if (isDynamiteExploding) {
			if(!colliderList.Contains(other)) {
				colliderList.Add (other);
			}
		}
	}

	private GameObject dynamiteTimeLeftText;
	void dynamiteTimeLeft() {
		if (timeLeftToExplode < 0.21F) {
			CancelInvoke ();
			dynamiteTimeLeftText.SetActive (false);
			return;
		}
		dynamiteTimeLeftText.GetComponent<Text>().text = Mathf.FloorToInt(timeLeftToExplode + 1).ToString ();
	}

	void OnTriggerExit2D(Collider2D other) {

		if (colliderList.Contains (other)) {
			colliderList.Remove (other);
		}
	}
}

