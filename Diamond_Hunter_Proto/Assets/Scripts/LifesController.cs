﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifesController : MonoBehaviour {

	public int life = 3;
	public int lifeDistance = 1;
	public GameObject lifePrefab; 
	private int lifeCount;
	public Sprite fullLife;
	public Sprite takenLife;
	public GameObject gameOverScreen;
	public float timeToGameOver;

	public bool getFullLifeInScene;

	private float runTimeGameOverLeft;


	[HideInInspector] public List<GameObject> lifes;

	// Use this for initialization
	void Start () {
		/*lifes = new List<GameObject> ();
		lifeCount = PlayerPrefs.GetInt ("lifes");
		if (getFullLifeInScene || lifeCount > life) {
			lifeCount = life;
		}
		//lifeCount = 2;
		print (lifeCount);
		for(int i = 0; i < life; i++) {
			Vector3 lifePosition = transform.position;
			lifePosition.x += i * lifeDistance;

			lifes.Add(Instantiate (lifePrefab, lifePosition, transform.rotation));
			if (i >= lifeCount) {
				lifes [i].GetComponent<SpriteRenderer> ().sprite = takenLife;
			}
			lifes [i].GetComponent<SpriteRenderer> ().color = new Color (1F, 1F, 1F, 0.7F);
		}*/
	}

	public void healthUpdate() {
		lifes = new List<GameObject> ();
		lifeCount = PlayerPrefs.GetInt ("lifes");
		if (getFullLifeInScene || lifeCount > life) {
			lifeCount = life;
		}

		//lifeCount = 2;
		//print(lifeCount);
		for(int i = 0; i < life; i++) {
			
			Vector3 lifePosition = transform.position;

			lifePosition.x += i * lifeDistance;

			lifes.Add(Instantiate (lifePrefab, lifePosition, transform.rotation));
			if (i >= lifeCount) {
				lifes [i].GetComponent<SpriteRenderer> ().sprite = takenLife;
			}
			lifes [i].GetComponent<SpriteRenderer> ().color = new Color (1F, 1F, 1F, 0.7F);
		}
	}

	public void damageTaken() {
		
		lifeCount--;
		if (lifeCount >= 0) {
			lifes [lifeCount].GetComponent<SpriteRenderer> ().sprite = takenLife;
			PlayerPrefs.SetInt("lifes", lifeCount);
			GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<ScreenShake> ().ShakeCamera ();
		} else {
			Debug.LogError ("DamageError!");
		}

		if (lifeCount <= 0) {

			GameObject.Find ("SceneManager").GetComponent<SceneManager> ().levelEnd (SceneManager.levelEndState.gameOver);
		}
	}

	public void healthAdded() {
		if (lifeCount >= life)
			return;

		lifeCount++;
		lifes [lifeCount - 1].GetComponent<SpriteRenderer> ().sprite = fullLife;
		PlayerPrefs.SetInt("lifes", lifeCount);

	}

	// Update is called once per frame
	void Update () {
		runTimeGameOverLeft -= Time.deltaTime;
	}
}
