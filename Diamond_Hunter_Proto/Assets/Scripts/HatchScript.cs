﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatchScript : MonoBehaviour {

	[SerializeField] private int gemsToOpen;
	[SerializeField] private GameObject winScreen;

	[SerializeField] private GameObject numbPrefab;
	[SerializeField] private PlayerTileController.dir numbPrefabDirection;
	[SerializeField] private Sprite[] numbSprites;
	private GameObject runTimeNumb; 


	Animator anim;
	//Animator anim2;
	Animator anim3;

	void Start() {
		anim = this.GetComponent<Animator> ();
		//anim2 = GameObject.Find ("Player").GetComponent<Animator> ();
		anim3 = GameObject.FindWithTag ("Enemy").GetComponent<Animator> ();
		if (this.name != "Hatch") {
			Debug.LogError ("Hatch has a false name!");
		}

		Vector3 numbPosition = transform.position;
		switch(numbPrefabDirection) {

		case PlayerTileController.dir.up:
			
			numbPosition.y++;
			runTimeNumb = Instantiate (numbPrefab, numbPosition, transform.rotation);
			break;

		case PlayerTileController.dir.left:
			
			numbPosition.x--;
			runTimeNumb = Instantiate (numbPrefab, numbPosition, transform.rotation);
			break;

		case PlayerTileController.dir.down:
			
			numbPosition.y--;
			runTimeNumb = Instantiate (numbPrefab, numbPosition, transform.rotation);
			break;

		case PlayerTileController.dir.right:
			
			numbPosition.x++;
			runTimeNumb = Instantiate (numbPrefab, numbPosition, transform.rotation);
			break;
		

		}
		runTimeNumb.GetComponent<SpriteRenderer> ().sprite = numbSprites[gemsToOpen - 1];
	}

	void Update() {
		
	}

	public void gemsInLineUpdate(int gemsInLine) {
		if (gemsInLine >= gemsToOpen) {
			runTimeNumb.GetComponent<SpriteRenderer> ().sprite = null;
			anim.SetBool ("Open", true);
		} else {
			runTimeNumb.GetComponent<SpriteRenderer> ().sprite = numbSprites [(gemsToOpen - gemsInLine) - 1];
			anim.SetBool ("Open", false);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player" && GemController.gemsInLine >= gemsToOpen) {
			other.gameObject.GetComponent<Animator> ().SetTrigger ("Berndlucke");

			Vector3 playerNewPosition = this.transform.position;
			playerNewPosition.x -= 0.6F;
			other.gameObject.transform.position = playerNewPosition;

			//anim2.SetTrigger ("Berndlucke");
			other.gameObject.GetComponent<PlayerTileController> ().moveSpeed = 0;
			GameObject.Find ("Player Glow").SetActive (false);
			GameObject.FindWithTag ("Enemy").GetComponent<EnemyController> ().enabled = false;
			//GameObject.FindWithTag ("Enemy").GetComponent<Animator> ().SetTrigger ("Escalation");
			anim3.SetTrigger ("Escalation");
			GameObject.Find ("SceneManager").GetComponent<SceneManager> ().levelEnd (SceneManager.levelEndState.win);
		}
	}


}
