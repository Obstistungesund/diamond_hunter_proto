﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZwischenScreenController : MonoBehaviour {

	public float timeToScoreCount;

	private Text levelsClearedText;
	private Text timeLeftText;
	private Text lastGemsText;
	private Text totalScoreText;
	private Text PressSpaceText;
	private string totalScoreTextBeforeNumb;

	private int levelsCleared;
	private int timeLeft;
	private int gems;
	private int lastScore;
	private int totalScore;

	private int runTimeScore;
	private int scoreDifference;

	bool displayDone;


	// Use this for initialization
	void Start () {
		displayDone = false;

		levelsCleared = PlayerPrefs.GetInt("level");
		timeLeft = PlayerPrefs.GetInt("lastTimeLeft");
		gems = PlayerPrefs.GetInt ("gems");
		lastScore = PlayerPrefs.GetInt("lastScore");
		totalScore = PlayerPrefs.GetInt("score");

		scoreDifference = totalScore - lastScore;


		levelsClearedText = GameObject.Find ("LevelClearedText").GetComponent<Text> ();
		levelsClearedText.text += levelsCleared;
		levelsClearedText.enabled = false;

		timeLeftText = GameObject.Find ("TimeLeftText").GetComponent<Text> ();
		timeLeftText.text += timeLeft;
		timeLeftText.enabled = false;

		lastGemsText = GameObject.Find ("GemsCollectedText").GetComponent<Text> ();
		lastGemsText.text += gems;
		lastGemsText.enabled = false;

		totalScoreText = GameObject.Find ("TotalScoreText").GetComponent<Text> ();
		totalScoreTextBeforeNumb = totalScoreText.text;
		totalScoreText.text += lastScore;
		totalScoreText.enabled = false;


		PressSpaceText = GameObject.Find ("PressSpaceText").GetComponent<Text> ();
		PressSpaceText.enabled = false;

		runTimeScore = lastScore;
		PlayerPrefs.SetInt ("score", totalScore);

		Invoke ("levelsClearedActive", 1.0F);
		//InvokeRepeating ("scoreAdd", 0.01F, 0.01F);

		scoreAdder = Mathf.RoundToInt (scoreDifference / (timeToScoreCount * 100));
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (displayDone) {
				nextLevel ();
			} else {
				setAllDisplays ();
			}
		}
	}

	void levelsClearedActive() {
		levelsClearedText.enabled = true;
		Invoke ("timeLeftActive", 1.0F);
	}

	void timeLeftActive() {
		timeLeftText.enabled = true;
		Invoke ("gemsActive", 1.0F);
	}

	void gemsActive() {
		lastGemsText.enabled = true;
		Invoke ("totalScoreActive", 1.0F);
	}

	void totalScoreActive() {
		totalScoreText.enabled = true;
		InvokeRepeating ("scoreAdd", 0.01F, 0.01F);
	}

	private bool spaceSwitch = false;
	void PressSpaceActive() {
		if (spaceSwitch) {
			PressSpaceText.enabled = true;
			spaceSwitch = false;
		} else {
			PressSpaceText.enabled = false;
			spaceSwitch = true;
		}
	}

	int scoreAdder;
	void scoreAdd() {
		
		runTimeScore += scoreAdder;
		if (runTimeScore >= totalScore) {
			runTimeScore = totalScore;
			CancelInvoke ();
			InvokeRepeating ("PressSpaceActive", 0.5F, .6F);
			displayDone = true;
		}
		totalScoreText.text = totalScoreTextBeforeNumb + " " + runTimeScore;
	}

	void setAllDisplays() {
		levelsClearedActive ();
		timeLeftActive();
		gemsActive();
		totalScoreText.enabled = true;
		totalScoreText.text = totalScoreTextBeforeNumb + " " + totalScore;
		CancelInvoke ();
		InvokeRepeating("PressSpaceActive", 0.5F, 0.6F);

		displayDone = true;
	}

	void nextLevel() {
		PlayerPrefs.SetInt ("lastScore", totalScore);
		GameObject.Find ("SceneManager").GetComponent<SceneManager> ().loadPlayableLevel ();
	}
}
