﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public AudioSource sfxSource;
	public AudioSource musicSource;
	public Vector2 sfxRandomPitchRange;
	public AudioClip gemTaken;
	public AudioClip diggingSound;
	public AudioClip laughSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void playDiggingSound() {
		playSFX (diggingSound);
	}

	public void playGemTaken() {
		playSFX (gemTaken);
	}

	public void playLaugh() {
		InvokeRepeating ("laughInvoke", 0.1F, laughSound.length);
	}

	int laughCount = 0;
	private void laughInvoke() {
		playSound(laughSound);
		laughCount++;

		if (laughCount > 1) {
			laughCount = 0;
			CancelInvoke ();
		}
	}

	private void playSFX(AudioClip SFX) {

		sfxSource.pitch = Random.Range (sfxRandomPitchRange.x, sfxRandomPitchRange.y);
		sfxSource.clip = SFX;
		sfxSource.Play ();
	}

	private void playSound(AudioClip sound) {
		sfxSource.pitch = 1;
		sfxSource.clip = sound;
		sfxSource.Play ();
	}

	public void escalatedMusic() {
		musicSource.pitch = 1.3F;
	}

}
