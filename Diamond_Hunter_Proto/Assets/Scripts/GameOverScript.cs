﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour {

	private Text scoreText;
	// Use this for initialization
	void Start () {
		scoreText = GameObject.Find ("YourScore").GetComponent<Text>();
		//print (PlayerPrefs.GetInt ("score"));
		scoreText.text = "Your Score: " + PlayerPrefs.GetInt("score");
	}


	public void tryAgain() {
		GameObject.Find ("SceneManager").GetComponent<SceneManager> ().newGame ();
	}

	public void mainMenu() {
		//Application.LoadLevel (0);
		//UnityEngine.SceneManagement.SceneManager.LoadScene ("MainMenu");
		GameObject.Find ("SceneManager").GetComponent<SceneManager> ().loadMainMenu ();
	}
}
