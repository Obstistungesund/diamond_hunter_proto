﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class Pathfinding : MonoBehaviour {
	
	public Vector2Int downLeftBorderPos;
	public Vector2Int topRightBorderPos;

	public Transform seeker, target;

	public int tilePenalty;

	public NodeGrid grid;

	private List<Node> path = new List<Node>();

	void Awake() {
		grid = new NodeGrid (downLeftBorderPos, topRightBorderPos, tilePenalty);
		seeker = this.transform;

		tilePenalty = PlayerPrefs.GetInt("level") / 5;
	}

	public Vector2Int nextPosition(Vector3 seeker, Vector3 target) {
		
		FindPath(new Vector2Int(Mathf.RoundToInt(seeker.x), Mathf.RoundToInt(seeker.y)), 
			     new Vector2Int(Mathf.RoundToInt(target.x), Mathf.RoundToInt(target.y)));

		if (path.Count == 0) {
			Vector2Int returnVector = new Vector2Int();
			returnVector.x = Mathf.RoundToInt (seeker.x);
			returnVector.y = Mathf.RoundToInt (seeker.y);
			return returnVector;
		}
		return path[0].worldPos;
	}


	private void FindPath(Vector2Int startPos, Vector2Int targetPos) {

		Node startNode = grid.nodeFromWorldPos(startPos);
		Node targetNode = grid.nodeFromWorldPos(targetPos);

		Heap<Node> openSet = new Heap<Node>(grid.maxSize);
		HashSet<Node> closedSet = new HashSet<Node>();
		openSet.Add(startNode);

		while (openSet.Count > 0) {
			Node currentNode = openSet.RemoveFirst();
			closedSet.Add(currentNode);

			if (currentNode == targetNode) {
				path = RetracePath(startNode,targetNode);
				return;
			}

			foreach (Node neighbour in grid.getNeighbours(currentNode)) {
				if (!neighbour.walkable || closedSet.Contains(neighbour)) {
					continue;
				}

				int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour) + neighbour.movementPenalty;
				if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour)) {
					neighbour.gCost = newMovementCostToNeighbour;
					neighbour.hCost = GetDistance(neighbour, targetNode);
					neighbour.parent = currentNode;
				
					if (!openSet.Contains(neighbour))
						openSet.Add(neighbour);
					else {
							openSet.UpdateItem(neighbour);
					}
				}
			}
		}
	}

	private List<Node> RetracePath(Node startNode, Node endNode) {
		List<Node> path = new List<Node>();
		Node currentNode = endNode;
	
		while (currentNode != startNode) {
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}
		path.Reverse();

		return path;

	}

	int GetDistance(Node nodeA, Node nodeB) {
		
		return Mathf.Abs (nodeA.worldPos.x - nodeB.worldPos.x) + Mathf.Abs (nodeA.worldPos.y - nodeB.worldPos.y);
	}


}