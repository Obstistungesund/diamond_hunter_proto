﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeGrid {

	private Vector2Int downLeftBorderPos;
	private Vector2Int topRightBorderPos;

	private Vector2Int gridSize;
	private Vector2Int gridOffset;

	[HideInInspector] public int maxSize;

	Node[,] grid; // 0,0 is downLeft

	void Awake() {
		
	}

	// Use this for initialization
	void Start () {
		
	}

	public NodeGrid(Vector2Int _downLeftBorderPos, Vector2Int _topRightBorderPos, int tilePenalty) {

		downLeftBorderPos = _downLeftBorderPos;
		topRightBorderPos = _topRightBorderPos;

		gridSize = new Vector2Int(Mathf.Abs (downLeftBorderPos.x - topRightBorderPos.x) - 1, Mathf.Abs (downLeftBorderPos.y - topRightBorderPos.y) - 1);
		gridOffset = new Vector2Int (downLeftBorderPos.x + 1, downLeftBorderPos.y + 1);
		grid = new Node[gridSize.x, gridSize.y];

		maxSize = gridSize.x * gridSize.y;

		//print (maxSize);

		for (int x = 0; x < gridSize.x; x++) {
			for (int y = 0; y < gridSize.y; y ++) {
				bool walkable = true;
				Vector2Int worldPoint = new Vector2Int (x + gridOffset.x, y + gridOffset.y);
				grid[x,y] = new Node(walkable, worldPoint, new Vector2Int(x, y), 0);
			}
		}

		foreach (GameObject wall in GameObject.FindGameObjectsWithTag("Wall")) {

			bool walkable = false;
			Vector2Int worldPoint = new Vector2Int(Mathf.RoundToInt(wall.transform.position.x), Mathf.RoundToInt(wall.transform.position.y));
			grid[worldPoint.x - gridOffset.x, worldPoint.y - gridOffset.y] = new Node(walkable, worldPoint, new Vector2Int(worldPoint.x - gridOffset.x, worldPoint.y - gridOffset.y), 0);

		}

		foreach (GameObject tile in GameObject.FindGameObjectsWithTag("Tile")) {

			bool walkable = true;
			Vector2Int worldPoint = new Vector2Int(Mathf.RoundToInt(tile.transform.position.x), Mathf.RoundToInt(tile.transform.position.y));
			grid[worldPoint.x - gridOffset.x, worldPoint.y - gridOffset.y] = new Node(walkable, worldPoint, new Vector2Int(worldPoint.x - gridOffset.x, worldPoint.y - gridOffset.y), tilePenalty);


		}

		//printGrid ();

	}

	public List<Node> getNeighbours(Node node) {


		List<Node> neighbours = new List<Node> ();
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				if ((x == 0 && y == 0) || (x == 1 && y == 1) || (x == 1 && y == -1) || (x == -1 && y == 1) || (x == -1 && y == -1))
					continue;

				int checkX = node.gridPos.x + x;
				int checkY = node.gridPos.y + y;

				if (checkX >= 0 && checkX < gridSize.x && checkY >= 0 && checkY < gridSize.y) {
					neighbours.Add(grid[checkX,checkY]);
				}
			}
		}

		return neighbours;
	}

	public Node nodeFromWorldPos(Vector2Int worldPos) {

		return grid [worldPos.x - gridOffset.x, worldPos.y - gridOffset.y];

	}

	/*public bool isWorldPosInGrid(Vector2Int worldPos) {
		
		if (worldPos.x - gridOffset.x < 0 || worldPos.x - gridOffset.x >= grid.GetLength (0) || worldPos.y - gridOffset.y < 0 || worldPos.y - gridOffset.y >= grid.GetLength (1)) {
			return false;
		}

		return true;
	}*/

	public void tileRemove(Vector3 worldPos) {

		grid [Mathf.RoundToInt(worldPos.x - gridOffset.x), Mathf.RoundToInt(worldPos.y - gridOffset.y)].movementPenalty = 0;

	}

	private void printGrid() {

		for (int y = 0; y < grid.GetLength (1); y++) {
			for (int x = 0; x < grid.GetLength (0); x++) {
				MonoBehaviour.print ("WorldPos: " + grid [x, y].worldPos + ", Index: " + grid [x, y].gridPos + ", Walkable: " + grid [x, y].walkable);
			}

		}

	}


}
