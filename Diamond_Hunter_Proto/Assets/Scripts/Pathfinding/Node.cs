﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : IHeapItem<Node> {
	
	public bool walkable;
	public Vector2Int worldPos;
	public Vector2Int gridPos;
	public int movementPenalty;

	//private Vector2 worldPosition;
	public int gCost;
	public int hCost;
	public Node parent;
	int heapIndex;

	public Node(bool _walkable, Vector2Int _worldPos, Vector2Int _gridPos, int _penalty) {

		walkable = _walkable;
		worldPos = _worldPos;
		gridPos = _gridPos;
		movementPenalty = _penalty;

	}

	/*public static Node getNodeFromWorldPos(Vector2Int nodePosition) {

		return new Node(true, new Vector2Int(), new Vector2Int());
	}*/
		
	public int fCost{
		get{
			return gCost + hCost;
		}
	}

	public int HeapIndex {

		get {
			return heapIndex;
		}
		set {
			heapIndex = value;
		}
	}

	public int CompareTo(Node nodeToCompare) {
		int compare = fCost.CompareTo(nodeToCompare.fCost);
		if (compare == 0) {
			compare = hCost.CompareTo(nodeToCompare.hCost);
		}
		return -compare;
	}

}
