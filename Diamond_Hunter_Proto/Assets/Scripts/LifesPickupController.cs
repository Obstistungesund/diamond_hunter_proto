﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifesPickupController : MonoBehaviour {

	[SerializeField] private int scoreValue;
	public GameObject scoreAdderPrefab;

	private GameObject pointAdder;

	LifesController levelLifes;
	// Use this for initialization
	void Start () {
		pointAdder = Instantiate (scoreAdderPrefab, this.transform.position, this.transform.rotation, GameObject.Find("LevelUI").transform);
		pointAdder.GetComponent<Text> ().text = scoreValue.ToString();
		pointAdder.SetActive (false);

		levelLifes = GameObject.Find ("Lifes").GetComponent<LifesController> ();
	}

	void OnTriggerEnter2D(Collider2D other) {

		if (other.tag == "Player") {
			levelLifes.healthAdded ();
			GameObject.Find("Score").GetComponent<ScoreManager>().scoreAdd (scoreValue);
			this.gameObject.SetActive (false);

			pointAdder.SetActive (true);
			pointAdder.transform.position = this.transform.position;
			InvokeRepeating ("pointAdderAdd", 0.01F, 0.01F);
		}

	}
		
	private float speed = 0.05F;
	private float test = 0.001F;
	private void pointAdderAdd() {
		speed -= test;
		Vector3 newPosition = pointAdder.transform.position;
		newPosition.y += speed;

		if (speed <= 0) {
			pointAdder.SetActive (false);
			speed = 0.05F;
			CancelInvoke ();
			return;
		}

		pointAdder.transform.position = newPosition;
	}

}
