﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blackScreenZoom : MonoBehaviour {
	
	[SerializeField] private float zoomSpeed;
	[SerializeField] private float acceleration;

	// Use this for initialization
	void Start () {
		
	}
	
	void zoomToPlayer() {
		GameObject.Find ("BlackScreen").GetComponent<SpriteRenderer> ().enabled = true;
		zoomSpeed += acceleration;
		Vector3 newScale = transform.localScale;

		newScale.x -= zoomSpeed;
		newScale.y -= zoomSpeed;

		if (newScale.x <= 0) {
			newScale.x = 0;
			newScale.y = 0;
			CancelInvoke ();
		}

		transform.localScale = newScale;
	}

	public void zoomToPlayerInvoke() {
		transform.position = GameObject.FindWithTag ("Player").transform.position;
		//newPos.z = 0;
		//transform.position = newPos;

		Vector3 newScale = transform.localScale;
		newScale.x = 10;
		newScale.y = 10;

		transform.localScale = newScale;
		print (transform.position.z);
		InvokeRepeating ("zoomToPlayer", 0.5F, 0.01F);
	}
}
