﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManager : MonoBehaviour {

	[SerializeField] private GameObject numbPrefab;
	[SerializeField] private Sprite[] numbSprites;
	[SerializeField] private int timeToStart;
	private GameObject runTimeNumb;

	Scene activeScene;

	void Awake() {
		
		activeScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene ();

		if (!(activeScene.name == "MainMenu" || activeScene.name == "GameOver" || activeScene.name == "Zwischenscene")) {
			
			GameObject.Find ("Timer").GetComponent<Countdown> ().enabled = false;
			//GameObject.Find ("Player").GetComponent<PlayerController> ().enabled = false;
			GameObject.Find ("Player").GetComponent<PlayerTileController> ().enabled = false;
			GameObject.Find ("Player").GetComponent<Animator> ().enabled = false;
			GameObject.FindWithTag ("Enemy").GetComponent<EnemyController> ().enabled = false;
			GameObject.FindWithTag ("Enemy").GetComponent<Animator> ().enabled = false;
			//PlayerPrefs.SetInt("score", 0);
			GameObject.Find ("Lifes").GetComponent<LifesController> ().healthUpdate ();

			foreach (GameObject gem in GameObject.FindGameObjectsWithTag("Gem")) {
				gem.GetComponent<GemController> ().enabled = false;
			}

			runTimeNumb = Instantiate (numbPrefab, transform.position, transform.rotation);
			Vector3 scale = runTimeNumb.transform.localScale;
			scale.x += 5;
			scale.y += 5;
			runTimeNumb.transform.localScale = scale;
			runTimeNumb.GetComponent<SpriteRenderer> ().sprite = numbSprites [timeToStart - 1];
			InvokeRepeating ("startCountdown", 1F, 1F);
		} else if (activeScene.name == "Zwischenscene") {
			GameObject.Find ("Lifes").GetComponent<LifesController> ().healthUpdate ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("MainMenu");
			//Application.LoadLevel (0);
		}
	}
		
	void startCountdown() {
		timeToStart--;
		if (timeToStart <= 0) {
			GameObject.Find ("Timer").GetComponent<Countdown> ().enabled = true;
			GameObject.Find ("Player").GetComponent<PlayerTileController> ().enabled = true;
			GameObject.Find ("Player").GetComponent<Animator> ().enabled = true;
			GameObject.FindWithTag ("Enemy").GetComponent<EnemyController> ().enabled = true;
			GameObject.FindWithTag ("Enemy").GetComponent<Animator> ().enabled = true;

			foreach (GameObject gem in GameObject.FindGameObjectsWithTag("Gem")) {
				gem.GetComponent<GemController> ().enabled = true;
			}
			Destroy (runTimeNumb);
			CancelInvoke ();
		} else {
			runTimeNumb.GetComponent<SpriteRenderer> ().sprite = numbSprites [timeToStart - 1];
		}
	}

	public enum levelEndState {gameOver, win};
	public void levelEnd(levelEndState endState) {

		GameObject.Find ("Score").GetComponent<ScoreManager> ().levelEnd ();
		PlayerPrefs.SetInt("gems", GemController.gemsInLine);

		if (endState == levelEndState.gameOver) {
			//Application.LoadLevel ("GameOver");
			//UnityEngine.SceneManagement.SceneManager.LoadScene ("GameOver");
			GameObject player = GameObject.FindWithTag("Player");
			player.GetComponent<Animator>().SetTrigger ("Dead");
			player.GetComponent<PlayerTileController>().enabled = false;
			player.GetComponent<SpriteRenderer>().sortingLayerName = "ScreenLayer";
			player.GetComponent<SpriteRenderer>().sortingOrder = 2;

			GameObject.FindWithTag ("Enemy").GetComponent<EnemyController> ().enabled = false;
			GameObject.Find("BlackScreenSpotLight").GetComponent<blackScreenZoom>().zoomToPlayerInvoke();
			Invoke("loadGameOver", 4.5F);
		} else if (endState == levelEndState.win) {
			PlayerPrefs.SetInt ("level", PlayerPrefs.GetInt ("level") + 1);
			Invoke("loadZwischenScene", 2F);
			//Application.LoadLevel (Random.Range(1, 5));
		}
	}

	public void loadPlayableLevel() {
		UnityEngine.SceneManagement.SceneManager.LoadScene (Random.Range(1, 5));
		//GameObject.Find ("Player").GetComponent<PlayerTileController> ().moveSpeed = 2.6f;
	}

	public void loadMainMenu() {
		UnityEngine.SceneManagement.SceneManager.LoadScene ("MainMenu");
	}

	public void loadGameOver() {
		UnityEngine.SceneManagement.SceneManager.LoadScene ("GameOver");
	}

	public void newGame() {
		PlayerPrefs.SetInt("gems", 0);
		PlayerPrefs.SetInt("lastScore", 0);
		PlayerPrefs.SetInt("level", 0);
		PlayerPrefs.SetInt("score", 0);
		PlayerPrefs.SetInt("lifes", 3);
		loadPlayableLevel ();
	}

	public void loadZwischenScene() {
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Zwischenscene");
	}
}
