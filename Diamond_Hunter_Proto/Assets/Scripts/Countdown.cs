﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
	public int countdown;
	public int escalateTime;

	public Text countdownText;
	public int timeLeft;
	private EnemyController enemy;
	private SoundManager soundManager;

	// Use this for initialization
	void Start()
	{
		enemy = GameObject.FindWithTag ("Enemy").GetComponent<EnemyController> ();
		soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager> ();
		timeLeft = countdown;
		StartCoroutine("LoseTime");
	}

	// Update is called once per frame
	void Update()
	{
		countdownText.text = ("Time Left : " + timeLeft);

		if (timeLeft <= 0)
		{
			StopCoroutine("LoseTime");
			countdownText.text = "Times Up!";
			//Application.LoadLevel ("GameOver");

			GameObject.Find ("SceneManager").GetComponent<SceneManager> ().levelEnd (SceneManager.levelEndState.gameOver);

		} else if(timeLeft < escalateTime) {
			soundManager.escalatedMusic ();
			enemy.isEscalated = true;
			countdownText.color = Color.red;
		}
	}

	IEnumerator LoseTime()
	{
		while (true)
		{
			yield return new WaitForSeconds(1);
			timeLeft--;
			PlayerPrefs.SetInt ("lastTimeLeft", timeLeft);
		}
	}
}