﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR 
using UnityEditor;
#endif

using System.IO;
using UnityEngine.UI;
using System;

public class HighScoreController : MonoBehaviour {

	private InputField nameField;
	private GameObject nameInputCanvas;
	private GameObject grayBackground;
	bool hasScore;
	//string userName;
	int level;
	int score;
	int rank;

	string[][] highScoresData;

	bool isInHighScore;

	string highScoreDirectory;
	// Use this for initialization
	void Start () {
		grayBackground = GameObject.Find ("GrayBackground");
		grayBackground.SetActive(false);

		rank = 0;
		highScoreDirectory = Directory.GetCurrentDirectory () + "/HighScores.txt";

		nameField = GameObject.Find ("InputField").GetComponent<InputField>();
		nameField.onEndEdit.AddListener(delegate {getName(); });

		nameInputCanvas = GameObject.Find ("InputNameCanvas");
		nameInputCanvas.SetActive (false);
		
		highScoresData = null;

		score = PlayerPrefs.GetInt ("score");
		level = PlayerPrefs.GetInt ("level");

		if (score <= 0) {
			hasScore = false;
		} else {
			hasScore = true;

		}

		List<string> highScoreLines = new List<string> ();

		try {
			
			if (File.Exists (highScoreDirectory)) {
				
				foreach (string line in File.ReadAllLines(highScoreDirectory)) {
					if(!String.IsNullOrEmpty(line)) {
						highScoreLines.Add (line);
					}
				}

				if(highScoreLines.Count > 0) {
					highScoresData = new string[highScoreLines.Count][];

					for (int i = 0; i < highScoreLines.Count; i++) {
						string[] splitLine = highScoreLines[i].Split(',');

						highScoresData[i] = splitLine;

					}
				}

				printHighScores();

			} else {
				
				//Debug.LogWarning ("WARNING: HIGHSCORE FILE NOT FOUND. CREATING NEW FILE.");
				string[] newHighScore = new string[1];
				newHighScore[0] = "";
				highScoreLines.Add("");
					
				File.WriteAllLines (highScoreDirectory, newHighScore);

			}
		} catch (IOException e) {
			Debug.LogException (e);
		} catch (MissingReferenceException r) {
			Debug.LogException (r);
		}
			
		if (hasScore) {

			bool isInHighScore = false;

			if (highScoresData == null || highScoresData.GetLength (0) <= 0) {

				isInHighScore = true;

			} else {
				int[] scores = new int[highScoresData.GetLength (0)];

				if (scores.Length <= 4) {
					isInHighScore = true;
				}
			
				for (int k = 0; k < scores.Length; k++) {
					//scores [i] = highScoresData [i, 2];
					Int32.TryParse(highScoresData [k][2], out scores[k]);
				}

				int i = 0;
				/*for (int k = 0; i < scores.Length; i++) {

					if (score > scores [i]) {
						isInHighScore = true;
						rank = i;
					}
				}*/

				while (i < scores.Length) {

					if (score > scores [i]) {
						isInHighScore = true;
						break;
					}

					i++;
				}
				rank = i;
			}

			if (isInHighScore) {
				// Get Name UI
				grayBackground.SetActive(true);
				nameInputCanvas.SetActive (true);
			} else {
				printHighScores ();
			}

		}

	}

	void getName() {

		string nameFieldText = nameField.text;

		if (String.IsNullOrEmpty (nameFieldText)) {

			Text signText = GameObject.Find ("SignText").GetComponent<Text>();
			signText.text = "Please Enter a Name!";
			signText.color = new Color (62F / 255.0F, 11.0F / 255F, 11.0F / 255.0F, 1);


			return;
		}

		if (nameFieldText.Length > 8) {
			// Zu langer Name
			Text signText = GameObject.Find ("SignText").GetComponent<Text>();
			signText.text = "Your Name is to long!";
			signText.color = new Color (62F / 255.0F, 11.0F / 255F, 11.0F / 255.0F, 1);

			return;
		}

		string[] newHighScoreString = new string[3];
		newHighScoreString [0] = nameFieldText;
		newHighScoreString [1] = level.ToString();
		newHighScoreString [2] = score.ToString();

		print (rank);
		highScoreSetter (rank, newHighScoreString);
		printHighScores ();
		setNewHighScoreText ();
		nameInputCanvas.SetActive (false);
		grayBackground.SetActive(false);

	}

	void printHighScores() {

		if (highScoresData == null || highScoresData.Length <= 0) {
			return;
		}

		for (int row = 0; row < highScoresData.GetLength (0); row++) {

			string rowName = "Row" + row;
			for (int col = 0; col < 3; col++) {

				switch (col) {

				case 0:
					GameObject.Find(rowName).transform.Find("Name").GetComponent<Text>().text = highScoresData[row][col];
					break;

				case 1:
					GameObject.Find(rowName).transform.Find("Level").GetComponent<Text>().text = highScoresData[row][col];
					break;

				case 2:
					GameObject.Find(rowName).transform.Find("Score").GetComponent<Text>().text = highScoresData[row][col];
					break;
				}
			}
		}
	}

	void setNewHighScoreText() {
		
		string[] highScoreTxtString;
		if (highScoresData == null || highScoresData.GetLength (0) == 0 ) {
			highScoreTxtString = new string[1];
			highScoreTxtString [0] = "";
		} else {
			highScoreTxtString = new string[highScoresData.GetLength (0)];
			for (int i = 0; i < highScoresData.GetLength (0); i++) {

				highScoreTxtString [i] = highScoresData [i] [0] + "," + highScoresData [i] [1] + "," + highScoresData [i] [2];

			}
		}
		File.WriteAllLines (highScoreDirectory, highScoreTxtString);
	}

	void highScoreSetter(int rank, string[] newScore) {
		if (newScore.GetLength (0) <= 0 || rank < 0 || rank > 4) {
			return;
		}

		if (highScoresData == null) {
			highScoresData = new string[1][];
			highScoresData [0] = newScore;
			return;
		}

		if (rank < highScoresData.GetLength (0)) {

			List<String[]> highScoreDataList = new List<String[]> (highScoresData);
			highScoreDataList.Insert (rank, newScore);
			print (highScoreDataList.Count);
			if (highScoreDataList.Count >= 6) {
				highScoreDataList.RemoveAt (5);
			}

			highScoresData = highScoreDataList.ToArray();

		} else {
			string[][] newHighScoresData = new string[rank + 1][];

			for (int i = 0; i < newHighScoresData.Length; i++) {
				if (i < highScoresData.Length) {
					newHighScoresData [i] = highScoresData [i];
				} else if (i == rank) {
					newHighScoresData [i] = newScore;
				}
			}

			highScoresData = newHighScoresData;
		}


	}


}
