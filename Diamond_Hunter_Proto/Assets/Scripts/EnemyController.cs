﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {


	//[SerializeField] private float distanceToMove = 1f;
	[SerializeField] private float moveSpeed = 3.5f;
	[SerializeField] private float speedMulti;
	[SerializeField] private float escalateMoveSpeed;
	//[SerializeField] private float moveSpeedOnDigging = 0.5f;
	[SerializeField] private float timeForDig;
	private float timeLeft;
	[SerializeField] private float timeForDigEscalated;
	[SerializeField] private float secondsOfDizziness = 5;
	[SerializeField] private int blocksToHideGem = 5;

	[HideInInspector] public float realSpeed;

	[HideInInspector] public static Vector3 playerWalkToPosition;
	[HideInInspector] public static Vector3 playerPosition;
	[HideInInspector] public static Vector3 lastGemPositionForwardPosition;
	[HideInInspector] public Vector3 spawnPoint;
	[HideInInspector] public List<Vector3> lastPositions;

	private bool isHidingGem;
	private GameObject gemPocket;

	private Vector3 target;

	private GameObject player;


	//private SpriteRenderer spriteRenderer;

	private Animator anim;

	private LifesController lifesController;
	private GameObject wall;
	private Vector3 endPosition;

	private float dizzinessTimeLeft;

	private Pathfinding AStar;

	private TileManager tileManager;
	private SoundManager soundManager;


	[HideInInspector] public bool isEscalated = false;

	void Start () {

		moveSpeed += PlayerPrefs.GetInt ("level") * speedMulti;

		tileManager = GameObject.Find ("TileManager").GetComponent<TileManager> ();
		timeLeft = 0;
		AStar = GetComponent<Pathfinding> ();
		player = GameObject.Find ("Player");

		soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager> ();
		dizzinessTimeLeft = 2.5F;
		soundManager.playLaugh(); 

		//spriteRenderer = this.GetComponent<SpriteRenderer> ();
		lifesController = GameObject.Find ("Lifes").GetComponent<LifesController>();
		realSpeed = moveSpeed;
		endPosition = transform.position;

		spawnPoint = transform.position;

		anim = GetComponent<Animator> ();

		isHidingGem = false;

		lastPositions = new List<Vector3>(blocksToHideGem);
		for (int i = 0; i < blocksToHideGem; i++) {

			lastPositions.Add (this.transform.position);

		}
	}
		
	//Test
	void FixedUpdate () {

		if (isEscalated) {
			anim.SetTrigger("Escalation");
		}

		if (endPosition == transform.position) {

			lastPositions.RemoveAt (0);
			lastPositions.Add(this.transform.position);
			//print (lastPositions.Count);

			if (isHidingGem) {
				anim.SetBool ("holdsGem", true);
				//print (target);
				if (transform.position == target) {
					dropGem ();
				}
			} else {
				target = player.transform.position;
			}

			Vector2Int nextPosition = AStar.nextPosition (this.transform.position, target);
			endPosition = new Vector3 (nextPosition.x, nextPosition.y, 0);

			if (nextPosition.x > transform.position.x) {
				anim.SetInteger ("Direction", 3);
			} else if (nextPosition.x < transform.position.x) {
				anim.SetInteger ("Direction", 1);
			} else if (nextPosition.y > transform.position.y) {
				anim.SetInteger ("Direction", 0);
			} else if (nextPosition.y < transform.position.y) {
				anim.SetInteger ("Direction", 2);
			}
		}

		dizzinessTimeLeft -= Time.fixedDeltaTime;
		timeLeft -= Time.fixedDeltaTime;

		if (dizzinessTimeLeft <= 0) {
			anim.SetBool ("isDigging", false);

			if (timeLeft <= -0.1F) {
				
				transform.position = Vector3.MoveTowards (transform.position, endPosition, (isEscalated ? escalateMoveSpeed : moveSpeed) * Time.deltaTime);

			} 
		}
			
	}


	public void dropGem() {
		if (gemPocket == null) {
			return;
		}
		isHidingGem = false;
		target = player.transform.position;
		gemPocket.transform.position = this.transform.position;
		gemPocket.SetActive(true);
		//gemPocket.GetComponent<SpriteRenderer> ().enabled = true;
		anim.SetBool ("holdsGem", false);
		gemPocket = null;

	}

	void OnTriggerStay2D(Collider2D other) {
		
		if (other.tag == "Tile" && timeLeft <= -0.1F) {

			StartCoroutine(invokeTileDmg(other.transform.position));
			anim.SetBool ("isDigging", true);
			if (isEscalated) {
				timeLeft = timeForDigEscalated;
			} else {
				timeLeft = timeForDig;
			}
			/*isOnWallTile = true;
			wall = other.gameObject;
			if (isEscalated) {
				realSpeed = escalateDiggingSpeed;
			} else {
				realSpeed = moveSpeedOnDigging;
			}*/
		} else if (other.tag == "GemLine") {
			
			if (dizzinessTimeLeft <= 0 && gemPocket == null && !isHidingGem) {
				anim.SetTrigger ("StealGem");
				//enemyStun ();
				//hideGem ();
				target = lastPositions[0];
				isHidingGem = true;
				gemPocket = other.gameObject;
				other.tag = "Gem";
				other.gameObject.SetActive(false);
				anim.SetTrigger ("StealGem");
				//lifesController.damageTaken ();


				GemController.removeFromLine(other.gameObject);
			}
		} else if (other.tag == "Player" && !isHidingGem) {
			if (dizzinessTimeLeft <= 0) {
				soundManager.playLaugh();
				anim.SetTrigger ("Attack");
				dizzinessTimeLeft = 2.5F;
				//enemyStun ();
				player.GetComponent<PlayerTileController> ().enemyHit ();
				lifesController.damageTaken ();
			}
		}
	}


	IEnumerator invokeTileDmg(Vector3 otherPosition)
	{
		
		yield return new WaitForSeconds(isEscalated ? timeForDigEscalated : timeForDig);

		tileManager.tileDmg (otherPosition);
		// Now do your thing here
	}


	private bool isSeeingPlayer() {

		RaycastHit2D hit = Physics2D.Raycast(transform.position, (player.transform.position - this.transform.position).normalized);
		//print (hit.collider.gameObject.tag);
		if (hit.transform.gameObject.tag == "Player") {
			return true;
		} else if (hit.transform.gameObject.tag == "Untagged") {
			print ("Untagged GameObject: " + hit.transform.gameObject.name);
		}

		return false;
	}


	public void enemyStun() {
		dizzinessTimeLeft = secondsOfDizziness;
	}

	public void dynamiteHit() {
		anim.SetTrigger ("DynamiteHit");

		//enemyStun ();
		dizzinessTimeLeft = secondsOfDizziness;
		Invoke ("posToSpawnPoint", 0.6F);
	}

	public void posToSpawnPoint() {
		this.transform.position = spawnPoint;

		Vector2Int nextPosition = AStar.nextPosition (this.transform.position, player.transform.position);
		endPosition = new Vector3 (nextPosition.x, nextPosition.y, 0);

		if (nextPosition.x > transform.position.x) {
			anim.SetInteger ("Direction", 3);
		} else if (nextPosition.x < transform.position.x) {
			anim.SetInteger ("Direction", 1);
		} else if (nextPosition.y > transform.position.y) {
			anim.SetInteger ("Direction", 0);
		} else if (nextPosition.y < transform.position.y) {
			anim.SetInteger ("Direction", 2);
		}
	}
		
}
