﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {

	[SerializeField] private int tileHealth;
	[SerializeField] private Sprite firstDamage;
	[SerializeField] private Sprite secondDamage;
	[SerializeField] private Sprite[] tileSprites;
	[SerializeField] private Sprite[] wallSprites;
	[SerializeField] private GameObject dmg;
	[SerializeField] private Vector2Int downLeftBorderPos;
	[SerializeField] private Vector2Int topRightBorderPos;

	[HideInInspector] public GameObject[,] worldGrid;
	private Vector2Int gridOffset;
	private Vector2Int gridSize;
	private int[,] gridTilesHealth;

	//List<GameObject> tiles;
	//List<int> tilesHealth;
	//List<GameObject> walls;
	//private PlayerController player;
	//private float timeForDig;

	private Pathfinding pathfinding;

	// Use this for initialization
	void Start () {
		
		if (this.name != "TileManager") {
			Debug.LogError ("TileManager has a false name!");
		}

		gridSize = new Vector2Int(Mathf.Abs (downLeftBorderPos.x - topRightBorderPos.x) - 1, Mathf.Abs (downLeftBorderPos.y - topRightBorderPos.y) - 1);
		gridOffset = new Vector2Int (downLeftBorderPos.x + 1, downLeftBorderPos.y + 1);
		worldGrid = new GameObject[gridSize.x, gridSize.y];
		gridTilesHealth = new int[gridSize.x, gridSize.y];

		List<GameObject> allBlocksList = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Tile"));
		allBlocksList.AddRange (GameObject.FindGameObjectsWithTag ("Wall"));
		allBlocksList.AddRange (GameObject.FindGameObjectsWithTag ("NoneTile"));

		for (int x = 0; x < gridSize.x; x++) {
			for (int y = 0; y < gridSize.y; y ++) {
				
				Vector3 worldPoint = new Vector3 (x + gridOffset.x, y + gridOffset.y, 0);
				bool isTileInScene = false;

				foreach (GameObject tile in allBlocksList) {
					if (tile.transform.position == worldPoint) {
						isTileInScene = true;
						worldGrid [x, y] = tile;

						if (tile.tag == "Tile") {
							Instantiate (dmg, tile.transform.position, tile.transform.rotation, tile.transform);
							gridTilesHealth [x, y] = tileHealth;

							int randomNumb = Random.Range (0, tileSprites.Length);
							tile.GetComponent<SpriteRenderer> ().sprite = tileSprites [randomNumb];

						} else if (tile.tag == "Wall") {

							int randomNumb = Random.Range (0, wallSprites.Length);
							tile.GetComponent<SpriteRenderer> ().sprite = wallSprites [randomNumb];

						} 
					}
				}

				if (!isTileInScene) {
					worldGrid[x, y] = Instantiate (new GameObject(), worldPoint, this.transform.rotation);
					worldGrid [x, y].tag = "NoneTile";
				}
			}
		}

		pathfinding = GameObject.FindObjectOfType<Pathfinding> ().GetComponent<Pathfinding> ();

	}

	
	public void tileDmg(Vector3 tilePosition, int dmg = 1) {

		Vector2Int tilePosition2D = Vector2Int.RoundToInt(tilePosition);
		//print (tilePosition2D.x + gridOffset.x);
		
		int x = tilePosition2D.x - gridOffset.x;
		int y = tilePosition2D.y - gridOffset.y;
		//print (tilePosition);
		GameObject searchedTile = worldGrid[x, y];

		if (searchedTile != null && (searchedTile.tag == "Tile" || searchedTile.tag == "Wall")) {
			
			gridTilesHealth[x, y] -= dmg;

			if (gridTilesHealth [x, y] <= 0) {
				
				pathfinding.grid.tileRemove (searchedTile.transform.position);

				//Destroy (searchedTile);
				searchedTile.tag = "NoneTile";
				searchedTile.SetActive(false);

				return;
			} else {

				if (searchedTile.tag == "Wall") {
					return;
				}

				if (gridTilesHealth [x, y] == 2) {

					searchedTile.transform.GetChild(0).GetComponent<SpriteRenderer> ().sprite = firstDamage;
					//searchedTile.GetComponentInChildren<SpriteRenderer> ().sprite = firstDamage;

				} else if (gridTilesHealth [x, y] == 1) {

					searchedTile.transform.GetChild(0).GetComponent<SpriteRenderer> ().sprite = secondDamage;
					//searchedTile.GetComponentInChildren<SpriteRenderer> ().sprite = secondDamage;

				}
		
				return;
			}

		} else {
			Debug.LogError ("TileDamageError! TilePosition = " + tilePosition2D);
		}
	}

	public void dynamiteExplosion(Vector2Int realExplosionPosition) {

		int x = realExplosionPosition.x - gridOffset.x;
		int y = realExplosionPosition.y - gridOffset.y;

		List<GameObject> explosionTileList = new List<GameObject>();

		if (x < 0 || x > worldGrid.GetLength (0) || y < 0 || y > worldGrid.GetLength (1)) {
			Debug.LogError ("Dynamite Error!");
			return;
		}


		explosionTileList.Add (worldGrid [x, y]);

		if (!(x + 1 >= worldGrid.GetLength (0))) {
			explosionTileList.Add (worldGrid [x + 1, y]);
		}

		if(!(x - 1 < 0) ) {
			explosionTileList.Add(worldGrid[x - 1, y]);
		}

		if (!(y + 1 >= worldGrid.GetLength (1))) {
			explosionTileList.Add (worldGrid [x, y + 1]);
		}

		if(!(y - 1 < 0)) {
			explosionTileList.Add(worldGrid[x, y - 1]);
		}


		foreach(GameObject tile in explosionTileList) {
			
			if (tile.tag == "Tile" || tile.tag == "Wall") {
				pathfinding.grid.tileRemove (tile.transform.position);

				//Destroy (tile);
				//tile.SetActive(false);
				tileDmg(tile.transform.position, 3);
			}
		}


	}

	public GameObject getGridFromWorldPoint(Vector3 tilePos) {
		return worldGrid [Mathf.RoundToInt (tilePos.x) - gridOffset.x, Mathf.RoundToInt (tilePos.y) - gridOffset.y];
	}

	public bool isWorldPosInWorldGrid(Vector3 worldPos) {

		if (worldPos.x - gridOffset.x < 0 || worldPos.x - gridOffset.x >= worldGrid.GetLength (0) || worldPos.y - gridOffset.y < 0 || worldPos.y - gridOffset.y >= worldGrid.GetLength (1)) {
			return false;
		}

		return true;
	}

}
