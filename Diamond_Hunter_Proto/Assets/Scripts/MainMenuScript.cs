﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour {


	private GameObject optionCanvas;

	public void Start() {
		GameObject.Find ("GrayBackground").GetComponent<Image>().enabled = false;
		optionCanvas = GameObject.Find ("OptionMenuCanvas");
		optionCanvas.SetActive (false);

	}

	public void exit() {
		Application.Quit();
	}

	public void startGame() {
		//Application.LoadLevel (Random.Range(1, scenes + 1));
		GameObject.Find("SceneManager").GetComponent<SceneManager>().newGame();
	}

	public void options() {
		GameObject.Find ("GrayBackground").GetComponent<Image>().enabled = true;
		optionCanvas.SetActive (true);
	}

	public void scores() {
		PlayerPrefs.SetInt ("score", 0);
		GameObject.Find("SceneManager").GetComponent<SceneManager>().loadGameOver();
	}
}
